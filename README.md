
#  Python Django

Learning from youtube channel By Corey Schafer

https://www.youtube.com/channel/UCCezIgC97PvUuR4_gbFUs5g

##  Terminal Commands

### Create Project and app
```bash
django-admin startproject demo1
python3 manage.py startapp blog
pip3 install Pillow --> required working with images
```
### Manage DB
```bash
python3 manage.py migrate
python3 manage.py createsuperuser
python3 manage.py runserver
```

Every time you change the database structure needs to run

```bash
python3 manage.py makemigrations
python3 manage.py sqlmigrate blog 001  --> To see and check the sql command
python3 manage.py migrate
```

Working with database in a shell

```
python3 manage.py shell               
Python 3.7.7 (default, Mar 10 2020, 15:43:33) 
[Clang 11.0.0 (clang-1100.0.33.17)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> from blog.models import Post
>>> from django.contrib.auth.models import User
>>> User.objects.first()
<User: giondo>
>>> User.objects.all()
<QuerySet [<User: giondo>, <User: testuser>]>
>>> 
>>> Post.objects.all()
<QuerySet []>
>>> user =  User.objects.get(id=1)
>>> 
>>> Post.objects.all()
<QuerySet []>
>>> user
<User: giondo>
>>> post_1 = Post(title='Blog 1',content='First Post content', author=user)
>>> Post.objects.all()
<QuerySet []>
>>> post_1.save()
>>> Post.objects.all()
<QuerySet [<Post: Post object (1)>]>
>>> 
>>> post.author.email
'email@email.com'
>>> post.date_posted
datetime.date(2020, 7, 13)
>>> user.post_set.all()
<QuerySet [<Post: Blog 1>, <Post: Blog 2>]>
>>> user.post_set.create(title='Blog 3', content='Third post')
<Post: Blog 3>
>>> Post.objects.all()
<QuerySet [<Post: Blog 1>, <Post: Blog 2>, <Post: Blog 3>]>
>>> exit()

```

### Install Crispy Forms

```bash
pip3 install django-crispy-forms
```
### Configure Crispy Forms

Edit {{PROJECTNAME}}/settings.py and add

```
INSTALLED_APPS = [
    'users.apps.UsersConfig',
    'crispy_forms', 
...


At the end of the file:
CRISPY_TEMPLATE_PACK  = "bootstrap4"
```


### Import json data

```bash
 python3 manage.py shell
Python 3.7.7 (default, Mar 10 2020, 15:43:33)
[Clang 11.0.0 (clang-1100.0.33.17)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> import json
>>> from blog.models import Post
>>>
>>> with open('posts.json') as f:
...     posts_json = json.load(f)
...
>>> for posts in posts_json:
...
...     post = Post(title=posts['title'], content=posts['content'], author_id=posts['user_id'])
...     post.save()
...
>>> exit()
```
Ref:
Django Doc for date

https://docs.djangoproject.com/en/2.0/ref/templates/builtins/#date