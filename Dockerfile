FROM nginx:latest
RUN apt-get update -y
RUN apt-get install python3-pip nginx -y
RUN rm /etc/nginx/conf.d/default.conf
COPY . /app
COPY extrasfiles/demo1.conf /etc/nginx/conf.d/demo1.conf
COPY extrasfiles/run.sh /app/run.sh
RUN chmod +x /app/run.sh
RUN pip3 install -r /app/requirements.txt
RUN cd /app/ && python3 manage.py collectstatic --noinput
CMD ["/app/run.sh"]